import React from "react";
import PropTypes from "prop-types";

export default class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showMobileNav: false,
        };

        this.navHamburger = React.createRef();

        this.toggleMobileNav = this.toggleMobileNav.bind(this);
    }

    /**
     * Get the correct class for the navigation.
     */
    getNavClasses() {
        let withBackground = this.props.withBackground ? "nav--with-background" : "";
        let showWithMobile = this.state.showMobileNav ? "nav--show-for-mobile " : "";

        return `nav ${withBackground} ${showWithMobile}`;
    }

    /**
     * Toggle the mobile navigation
     */
    toggleMobileNav() {
        this.navHamburger.current.classList.toggle("active");

        this.setState({showMobileNav: !this.state.showMobileNav});
    }

    render() {
        return (
            <nav className={this.getNavClasses()}>
                <div className={`nav__item ${this.props.activeRoute === "home" ? 'nav__item--active' : ''}`}>
                    <a className="nav__link" href="#home-page">Home</a>
                    <div className="nav__underscore" />
                </div>
                <div className={`nav__item ${this.props.activeRoute === "about" ? 'nav__item--active' : ''}`}>
                    <a className="nav__link" href="#about-page">About</a>
                    <div className="nav__underscore" />
                </div>
                <div className={`nav__item ${this.props.activeRoute === "projects" ? 'nav__item--active' : ''}`}>
                    <a className="nav__link" href="#projects-page">Projects</a>
                    <div className="nav__underscore" />
                </div>
                <div className={`nav__item ${this.props.activeRoute === "contact" ? 'nav__item--active' : ''}`}>
                    <a className="nav__link" href="#contact-page">Contact</a>
                    <div className="nav__underscore" />
                </div>

                {!this.state.showMobileNav && <div className="nav-mobile-current-route">
                    {`${this.props.activeRoute.charAt(0).toLocaleUpperCase()}${this.props.activeRoute.slice(1)}`}
                </div>}

                <div className="hamburger" onClick={this.toggleMobileNav} ref={this.navHamburger}>
                    <div />
                </div>

                <ul className={`nav-mobile  ${this.state.showMobileNav ? "nav-mobile--show" : ""}`}>
                    <li className="mobile-item" onClick={this.toggleMobileNav}>
                        <div className="mobile-item__triangle" />
                        <a href="#home-page">Home</a>
                    </li>
                    <li className="mobile-item" onClick={this.toggleMobileNav}>
                        <div className="mobile-item__triangle" />
                        <a href="#about-page">About</a>
                    </li>
                    <li className="mobile-item" onClick={this.toggleMobileNav}>
                        <div className="mobile-item__triangle" />
                        <a href="#projects-page">Projects</a>
                    </li>
                    <li className="mobile-item" onClick={this.toggleMobileNav}>
                        <div className="mobile-item__triangle" />
                        <a href="#contact-page">Contact</a>
                    </li>
                </ul>
            </nav>
        );
    }
}

NavBar.propTypes = {
    activeRoute: PropTypes.string,
    withBackground: PropTypes.bool,
};

NavBar.defaultProps = {
    activeRoute: "home",
    withBackground: false,
};
