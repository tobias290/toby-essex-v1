import React from "react";
import PropTypes from "prop-types"

const ProgressBar = (props) =>
    <div className="progress-bar">
        <div className="progress-bar__icon">
            <i className={props.logoClassName} />
            <span>{props.label}</span>
            </div>
        <div className="progress-bar__bar">
            <div className={`progress-bar__filler progress-bar__percentage-${props.percentage}`} />
            <span className="progress-bar__percentage-label">{props.percentage}%</span>
        </div>
    </div>;

ProgressBar.propTypes = {
    label: PropTypes.string,
    logoClassName: PropTypes.string,
    percentage: PropTypes.number,
};

ProgressBar.defaultProps = {
    percentage: 100,
};

export default ProgressBar;
