import React from "react";

const Contact = () =>
    <div id="contact-page" className="full black-background">
        <h1 id="contact-title" className="page-title">Contact <div /></h1>
        <div id="contact-page-inner">
            <form className="form" action="mailto:tobysx@gmail.com" method="post" encType="text/plain">
                <input className="form__input" type="text" name="name" placeholder="Name" />
                <input className="form__input" type="text" name="mail" placeholder="Email" />
                <input className="form__input" type="text" name="subject" placeholder="Subject" />
                <textarea className="form__textarea" name="comment" placeholder="Your message..." />
                <input className="form__submit form__submit--float-right" type="submit" value="Send" />
            </form>
        </div>
    </div>;


export default Contact;

