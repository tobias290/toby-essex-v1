import React from "react";

const ToTopButton = () =>
    <div className="to-top-button-container">
       <a href="#home-page"><i className="fas fa-chevron-up" /></a>
    </div>;

export default ToTopButton ;
