import React from "react";

const About = () =>
    <article id="about-page" className="full black-background container">
        <section className="about-section">
            <h1 className="page-title page-title--no-top-margin">About Me<div /></h1>
            <p className="personal-statement">
                I am a Web Design & Development graduate from Edge Hill University, and I have a passion for application development across multiple platforms such as mobile, web and
                desktop. I am a confident developer across multiple languages such as Python, Java and PHP as well
                as experience in databases. For examples of my work/projects check out my <a className="inline" href="https://github.com/tobias290" target="_blank" rel="noreferrer noopener">GitHub</a> page and <a className="inline" href="https:///bitbucket.org/tobias290/" target="_blank" rel="noreferrer noopener">BitBucket</a> page.

                Building on my experience since I started six form college and enjoy working either as part of a
                team or independently. During my time at university I have been chosen as the team leader for many
                of the projects we have undertaken.
            </p>
        </section>

        <section className="about-section">
            <h2 className="page-title page-title--sub-title page-title--no-top-margin">My Skills<div /></h2>
            <div className="skills">
                <div className="skill">
                    <i className="fab fa-html5 skill__icon" />
                    <span className="skill__text">HTML 5</span>
                </div>
                <div className="skill">
                    <i className="fab fa-css3 skill__icon" />
                    <span className="skill__text">CSS</span>
                </div>
                <div className="skill">
                    <i className="fab fa-js-square skill__icon" />
                    <span className="skill__text">JS/Node</span>
                </div>
                <div className="skill">
                    <i className="fab fa-python skill__icon" />
                    <span className="skill__text">Python</span>
                </div>
                <div className="skill">
                    <i className="fab fa-php skill__icon" />
                    <span className="skill__text">PHP</span>
                </div>
                <div className="skill">
                    <i className="fas fa-database skill__icon" />
                    <span className="skill__text">SQL</span>
                </div>
            </div>
        </section>

        <section className="about-section">
            <h2 className="page-title page-title--sub-title page-title--no-top-margin">What I Use<div /></h2>
            <div className="skills">
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/jetbrains.png" alt="JetBrains" />
                    <span className="skill__text">JetBrains</span>
                </div>
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/visual_studio_white_border.png" alt="Visual Studio" />
                    <span className="skill__text">Visual Studio</span>
                </div>
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/visual_studio_code.png" alt="Visual Studio Code" />
                    <span className="skill__text">Atom</span>
                </div>
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/xcode.png" alt="X Code" />
                    <span className="skill__text">XCode</span>
                </div>
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/android_studio.png" alt="Android Studio" />
                    <span className="skill__text">Android Studio</span>
                </div>
                <div className="skill skill--purple">
                    <img className="skill__icon skill__icon--image" src="images/logos/xd.png" alt="Adobe XD" />
                    <span className="skill__text">Adobe XD</span>
                </div>
            </div>
        </section>

        <section className="about-section">
            <h2 className="page-title page-title--sub-title page-title--no-top-margin">Download My CV<div /></h2>

            <button id="cv-button" className="button">
                <a className="button__link" href="CV.docx" download>
                    <i className="fas fa-file-download"/>
                    &nbsp;
                    Download CV
                </a>
            </button>
        </section>
    </article>;

export default About;
