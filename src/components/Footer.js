import React from "react";


const Footer = () =>
    <footer className="footer">
        <div className="footer__copyright">Toby Essex &copy; 2019</div>

        <div className="footer__links">
            <a href="https://github.com/tobias290" target="_blank" rel="noreferrer noopener" title="GitHub"><i className="fab fa-github-square" /></a>
            <a href="https://www.facebook.com/toby.essextoby" target="_blank" rel="noreferrer noopener" title="Facebook"><i className="fab fa-facebook" /></a>
            <a href="https://www.linkedin.com/in/toby-essex-444a12160" target="_blank" rel="noreferrer noopener" title="LinkedIn"><i className="fab fa-linkedin" /></a>
        </div>

        <div className="footer__contact">
            Email: &nbsp;&nbsp;&nbsp;<span>tobysx@gmail.com</span>
            <br />
            Mobile: &nbsp;<span>07576 613361</span>
        </div>
    </footer>;

export default Footer;
