import React from "react";
import PropTypes from "prop-types";

const Project = (props) =>
    <a className="project" href={props.url} target="_blank" rel="noopener noreferrer" title="Click to view repository">
        <div className="project__name">{props.name}</div>
        <p className="project__description">{props.description}</p>
    </a>;

Project.propTypes = {
    name: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
};

export default Project;
