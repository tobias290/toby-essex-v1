import React from "react";
import Project from "./Project";


export default class About extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            repos: [],
            viewMore: false,
        }
    }

    /**
     * Called on page load/reload.
     *
     * Gets the repos from the GitHub api.
     */
    componentDidMount() {
        fetch("https://api.github.com/users/tobias290/repos")
            .then(resp => resp.json())
            .then(gitHubRepos => {
                fetch("https://api.bitbucket.org/2.0/repositories/tobias290")
                    .then(resp => resp.json())
                    .then(bitBucketRepos => this.setState({
                        repos: [...gitHubRepos, ...bitBucketRepos.values],
                        loading: false
                    }));
            });
    }

    render() {
        return (
            <div id="projects-page" className="full black-background container">
                <h1 id="contact-title" className="page-title">Projects <div /></h1>

                <div id="projects-page-inner">
                    {
                        !this.state.loading &&
                        (this.state.viewMore ? this.state.repos : this.state.repos.slice(0, 10)).map((repo, i) => {
                            return <Project
                                key={i}
                                name={repo.name}
                                description={repo.description}
                                url={repo.svn_url || repo.links.html.href}
                            />;
                        })
                    }
                </div>
                <button id="view-more" className="button" onClick={() => this.setState({viewMore: !this.state.viewMore})}>
                    {this.state.viewMore ? "View Less" : "View More"}
                </button>
            </div>
        );
    }
}

