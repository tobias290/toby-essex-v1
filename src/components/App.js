import React from "react";

import NavBar from "./NavBar";
import Contact from "./Contact";
import Home from "./Home";
import About from "./About";
import Footer from "./Footer";
import Projects from "./Projects";
import ToTopButton from "./ToTopButton";

import "../css/styles.css"

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeRoute: "home",
            navBarNeedsBackground: false,
            enableToTopButton: false,
        };

        this.onScroll = this.onScroll.bind(this);
    }

    /**
     * Called when the user scrolls.
     */
    onScroll() {
        // If the website has scrolled past the home page, enable the scroll button
        this.setState({
            enableToTopButton: document.getElementById("home-page").getBoundingClientRect().bottom <= 70
        });

        // Get the items that the nav bar represents
        let items = [
            ["home", document.getElementById("home-page").getBoundingClientRect()],
            ["about", document.getElementById("about-page").getBoundingClientRect()],
            ["projects", document.getElementById("projects-page").getBoundingClientRect()],
            ["contact", document.getElementById("contact-page").getBoundingClientRect()],
        ];

        // Loop over each item
        // Then check the scroll position of the website and activate the correct nav bar item
        for (let item of items)
            if (item[1].top <= 0)
                this.setState({activeRoute: item[0]});
    }

    render() {
        return (
            <div id="main-container" className="full" onScroll={this.onScroll}>
                <NavBar activeRoute={this.state.activeRoute} withBackground={this.state.enableToTopButton} />

                <Home />
                <About/>
                <Projects/>
                <Contact/>
                {this.state.enableToTopButton && <ToTopButton/>}
                <Footer/>
            </div>
        );
    }
}

