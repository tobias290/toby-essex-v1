import React from "react";

const Home = () =>
    <div id="home-page" className="full container">
        <div id="home-page-inner">
            <h1 className="jumbotron">Hi, I'm Toby Essex.</h1>
            <button className="button">
                <a className="button__link" href="#about-page">About Me</a>
            </button>
        </div>
    </div>;

export default Home;

